package com.redbanners.networkspider;

import com.redbanners.dispatcher.TodaySpiderDispatcher;
import com.redbanners.utils.FastIOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@SpringBootApplication
@EnableScheduling/* SpringBoot 定时框架 1.在入口程序添加注解 @EnableScheduling */
public class NetworkspiderApplication {

    private static final Logger logger = LoggerFactory.getLogger(NetworkspiderApplication.class);

    public static void main(String[] args) {
		SpringApplication.run(NetworkspiderApplication.class, args);
	}

    @Scheduled(cron = "0 0 0,1,10,22,23 * * ?") // cron 表达式  Test * * * * * ?     RunTime 0 0 0,1,10,22,23 * * ?
    private void startSpiderTroops() throws Exception {
        logger.info(FastIOUtils.sysDate("yyyy年MM月dd日HH时")+ " :爬虫革命爆发 !");

            TodaySpiderDispatcher.todayDispatcher();
            logger.info(FastIOUtils.sysDate("yyyy年MM月dd日HH时")+" :爬虫革命进入低潮 !");
        }
    }

